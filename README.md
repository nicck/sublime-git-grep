## Installation

    cd ~/Library/Application\ Support/Sublime\ Text\ 2/Packages/
    git clone git@bitbucket.org:nicck/sublime-git-grep.git GitGrep

Restart Sublime.

Menu: Find > Git Grep - ^⇧G

## TODO:

  - tests
  - truncated long lines in results
  - move process to background
  - show results on new tab
