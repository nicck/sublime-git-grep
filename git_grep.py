import commands, os
import sublime, sublime_plugin

class GitGrepCommand(sublime_plugin.WindowCommand):
    def __init__(self, window):
        self.last_query = ''
        if hasattr(sublime_plugin.WindowCommand, '__init__'):
            sublime_plugin.WindowCommand.__init__(self, window)

    def run(self):
        self.window.show_input_panel('git grep', self.last_query, self.on_done, self.on_change, self.on_cancel)

    def on_done(self, query):
        self.last_query = query

        fname = self.window.active_view().file_name()
        path = self.git_root(os.path.dirname(fname))

        sublime.status_message(path)

        os.chdir(path)
        status, out = commands.getstatusoutput('pwd')
        sublime.status_message("git grep in %s ..." % out)

        status, out = commands.getstatusoutput('git grep -in "%s"' % query)
        self.out_list = out.decode('utf8', 'ignore').split("\n")

        def split(l):
            fname, line, match = l.split(":", 2)
            return [match.strip()[0:100], ":".join([fname, line])]

        items = map(split, self.out_list)
        self.window.show_quick_panel(items, self.on_done_sel)

    def on_done_sel(self, index):
        if index == -1:
            return

        line = self.out_list[index]
        filename, lineno, match = line.split(":", 2)
        filename = os.path.abspath(filename)
        self.window.open_file(filename + ':' + lineno, sublime.ENCODED_POSITION)

    def on_change(self, arg):
        return

    def on_cancel(self):
        return

    def git_root(self, directory):
        while directory:
            if os.path.exists(os.path.join(directory, '.git')):
                return directory
            parent = os.path.realpath(os.path.join(directory, os.path.pardir))
            if parent == directory:
                # /.. == /
                return False
            directory = parent
        return False
